function Item(name, sell_in, quality) {
  this.name = name;
  this.sell_in = sell_in;
  this.quality = quality;
}

var items = []


function update_quality() {
  for (var i = 0; i < items.length; i++) {
    if (items[i].name == 'Sulfuras, Hand of Ragnaros') { // The Sulfuras legendary item stays the same quality 
        items[i].quality = items[i].quality;
    } else {
        if (items[i].quality >= 0 || items[i].name == 'Aged Brie' || items[i].name == 'Backstage passes to a TAFKAL80ETC concert') { //All other items than Sulfuras that are also greater than or equal to 0 quality, making sure to keep the ones that increase in value in the mix because they could start at 0
            items[i].sell_in = items[i].sell_in - 1; //sell in date decreases daily
            if (items[i].name == 'Backstage passes to a TAFKAL80ETC concert' || items[i].name == 'Aged Brie') { //Brie and passes greater than 0
                items[i].quality = items[i].quality + 1; //Quality increases
                if (items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
                     if (items[i].sell_in < 11) { //When the sell in date for backstage passes is at 10 and lower
                      items[i].quality = items[i].quality + 2; //Quality increases by 2
                        if (items[i].sell_in < 6) { //When the sell in date for backstage passes is at 5 and lower
                          items[i].quality = items[i].quality + 3; //Quality increases by 3
                            if (items[i].sell_in < 0) { //When passes hit the concert date
                                items[i].quality = items[i].quality - items[i].quality; //Quality goes to 0
                            }
                        }
                    }
                }
            } else if (items[i].name == 'Conjured Mana Cake') { //When the item is "Conjured"
                items[i].quality = items[i].quality - 2; //Quality decreases by 2
                if (items[i].sell_in < 0) { //When sell in date hits 0
                   items[i].quality = items[i].quality - 4;//Quality decreases twice as fast at 4
                }
            } else  { //All other items
                items[i].quality = items[i].quality - 1; //The quality decreases every sell in date
                if (items[i].sell_in < 0) { //When sell in date hits 0
                   items[i].quality = items[i].quality - 2;//Quality decreases twice as fast at 2
                }
            }
        }
        if (items[i].quality > 50) {
            items[i].quality = 50; //If quality of an item that isn't legendary is greater than 50 then set it to 50.
        } else if (items[i].quality < 0) {
            items[i].quality = 0; //If quality of an item that isn't legendary is less than 0 then set it to 0.
        }
    }
  }
}
